<?php
function tentukan_nilai($number)
{
    if($number >= 85){
        echo "Sangat Baik : ";
    }
    else if($number >= 70){
        echo "Baik : ";
    }
    else if ($number >= 60){
        echo "Cukup : ";
    }
    else {echo "Kurang : ";
    }
    return $number . "<br>";
    //  kode disini
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>